# Ejemplos pybullet


## base.py

Ejemplo sencillo para mostrar la configuración del mundo y como cargar diferentes urdf. Esta aplicación permite pasar como parámetro el urdf a cargar

```
$ python3 base.py 
pybullet build time: Nov 28 2023 23:45:17
usage: base.py [-h] --urdf URDF
```

Puedes cargar diferentes urdf con diferente configuración que se encuentran en el directorio urdf:
* urdf/base.urdf: Definición de la estructura visual
* urdf/base2.urdf: Definición de la estructura visual y de colisión
* urdf/base3.urdf: Modelado de un sistema que contiene 2 links y 1 joint.

![alt text](images/base3.png)

## example_continuous.py

Ejemplo de como configurar varios links y joints con una configuración "continua" para simular giros sobre un eje. Además tiene código de ejemplo de como añadir sliders de control en la interfaz gráfica para depurar y manejar los joints.

![alt text](images/example_continuous.png)




## example_prismatic.py

Ejemplo de como configurar varios links y joints con una configuración "prismatic" para simular desplazamientos sobre un eje.

![alt text](images/example_prismatic.png)


## example_friction_door.py

Ejemplo de como simular la velocidad de un robot, y el torque y fricción de una puerta.


![alt text](images/friction_door.png)


## example_friction.py

Ejemplo de simulación de un esfera y como aplica los diferentes coeficientes de rozamiento, lineales, giro y rodadura. Además se puede modificar el parámetro de restitución para poder simular rebotes.

![alt text](images/friction_sphere.png)